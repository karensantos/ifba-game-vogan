"use strict";
let stats
let padAngle = 0
let nivel = 0
const {sqrt, sin, cos, abs, sign, PI} = Math
const voganW = 3.5, voganH = 7, bolaRaio = 1, planetaRaio = 10, gravidade = .01
const padHalfArc = 0.26 //rad
const padHalfW =  abs(planetaRaio * sin(padHalfArc))
let points = 0
let userName = ''
let initPlayer = false

bola.vidas = 3
bola.x = 0
bola.y = - planetaRaio - bolaRaio
bola.vx = -.8
bola.vy = -.2

function movePadRight(){
  pad.style.transform = `rotate(${padAngle+=.3}rad)`
}


function movePadLeft(){
  pad.style.transform = `rotate(${padAngle-=.3}rad)`
}

btnplayerL.addEventListener("click", movePadLeft)
btnplayerR.addEventListener("click", movePadRight)

document.onkeydown = press;

function press(event) {
    event = event || window.event;
    if (event.keyCode == '37') {
       movePadLeft()
    }
    else if (event.keyCode == '39') {
       movePadRight()
    }
}

function startVogans(){
  for(let ang = 0; ang < 2*PI; ang+= PI/11){
    let vogan = document.createElement('u')
    espaco.appendChild(vogan)
    vogan.a = ang
    vogan.h = -19
    vogan.dano = 0
  }
  
  for(let ang = 0; ang < 2*PI; ang+= PI/19){
    let vogan = document.createElement('u')
    espaco.appendChild(vogan)
    vogan.a = ang
    vogan.h = -28
    vogan.dano = 0
  }
}

function getVogans(){
  return [...document.querySelectorAll('u')]
}

function updateVogans() {
  getVogans().map(vogan => {
    vogan.a += .001
    vogan.style.transform = `rotate(${vogan.a}rad) translateY(${vogan.h}em)`
  })
}

function initLifes(qtd){
  for(let v = 0; v < qtd; v+=1 ){
    let vida = document.createElement('li')
    vidas.appendChild(vida).append('🛰')
  }
}

initLifes(bola.vidas)

function hypotenuse(x, y) {
  return sqrt(x*x + y*y)
}
// distância entre cordenadas e retorna um vetor
function calcDist(p1, p2) {
  let dX = p1.x - p2.x
  let dY = p1.y - p2.y
  let dist = hypotenuse(dX, dY)
  return [dX, dY, dist, { x:dX/dist, y:dY/dist }]
}


function gameLoop(){
  stats && stats.begin() // DEBUG
  requestAnimationFrame(gameLoop)
  // atualização bola
  let [dX, dY, dist, vet] = calcDist({ x: 0, y: 0 }, bola)
  bola.vx += vet.x * gravidade
  bola.vy += vet.y * gravidade
  bola.x += bola.vx
  bola.y += bola.vy

  // y+ embaixo y- na parte de cima
  bola.style.top = bola.y +'em'
  bola.style.left = bola.x + 'em'

  updateVogans()
  testColision()
  testColisionPad()
  stats && stats.end() // DEBUG
}

function startgame(){
  espaco.style.display = 'none'
  start.addEventListener("click", ()=>{
    initPlayer = true
    espaco.style.display = 'block'
    inicio.style.display = 'none'
    startVogans()
    gameLoop()
  });
}

startgame()


function testColision() {
  const halfW = voganW / 2 + bolaRaio , halfH = voganH / 2 + bolaRaio
  getVogans().map(vogan => {
    const { a, h} = vogan
    // Rotation
    let xt  = bola.x  * cos(-a)  -  bola.y  * sin(-a)
    let yt  = bola.x  * sin(-a)  +  bola.y  * cos(-a)
    let vxt = bola.vx * cos(-a)  -  bola.vy * sin(-a)
    let vyt = bola.vx * sin(-a)  +  bola.vy * cos(-a)
    // Translation
    yt -= h
    if (xt > -halfW && xt < halfW && yt > -halfH && yt < halfH ) {
      vogan.dano++
      vogan.className = 'attacked dano-' + vogan.dano
      points+=5
      document.getElementById('pontos').innerHTML = '<p>'+points+'</p>'
      if(vogan.dano >=3){
        points+=10
        document.getElementById('pontos').innerHTML = '<p>'+points+'</p>'
        vogan.remove()
      }
      // 'quicar'
      if(abs(yt) > halfW){
        vyt *= -1
        yt = sign(yt) * (halfH + bolaRaio)
      } else{
        vxt *= -1
        xt = sign(xt) * (halfW + bolaRaio)
      }
      //atualização da posição da bola
      yt += h
      bola.x  = xt  * cos(a)  -  yt  * sin(a)
      bola.y  = xt  * sin(a)  +  yt  * cos(a)
      bola.vx = vxt * cos(a)  -  vyt * sin(a)
      bola.vy = vxt * sin(a)  +  vyt * cos(a)
    }
  })
}

function testColisionPad() {
  // Rotation
  let xt  = bola.x  * cos(-padAngle)  -  bola.y  * sin(-padAngle)
  let yt  = bola.x  * sin(-padAngle)  +  bola.y  * cos(-padAngle)
  let vxt = bola.vx * cos(-padAngle)  -  bola.vy * sin(-padAngle)
  let vyt = bola.vx * sin(-padAngle)  +  bola.vy * cos(-padAngle)

  const halfPad = padHalfW + bolaRaio/2
  //detecta colisão no pad
  if (xt > -padHalfW && xt < padHalfW && (yt - bolaRaio) < (planetaRaio + 0.5) ) {
    vyt *= -1
    // TODO: normalizar vetor velocidade
    bola.x  = xt  * cos(padAngle)  -  yt  * sin(padAngle)
    bola.y  = xt  * sin(padAngle)  +  yt  * cos(padAngle)
    bola.vx = vxt * cos(padAngle)  -  vyt * sin(padAngle)
    bola.vy = vxt * sin(padAngle)  +  vyt * cos(padAngle)
  }

  if(abs(yt) < planetaRaio){
    // perdeu a bola - abate das vidas
    bola.y = -35
    bola.vy = 0
    bola.vx = 0
    // melhorar implementaçãodocument.getElementById('vidas').lastChild.style.display = 'none' 
  }
}

import('https://mrdoob.github.io/stats.js/build/stats.module.js').then(mod => {
  const Stats = mod.default
  stats = new Stats()
	document.body.appendChild(stats.dom)
  ;[...stats.dom.children].map(c => {
    c.style.position = 'relative'
    c.style.display = 'inline-block'
  })
})
